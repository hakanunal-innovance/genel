/* eslint-disable complexity */
import { useState, useEffect } from 'react';

import { DynamicType } from '@models/model';

// Define general type for useWindowSize hook, which includes width and height
interface Size {
  width: number | undefined;
  height: number | undefined;
}

interface Theme {
  sm: number;
  md: number;
  lg: number;
  xl: number;
}
interface ThemeBoolean {
  base: DynamicType;
  sm: DynamicType;
  md: DynamicType;
  lg: DynamicType;
  xl: DynamicType;
}

const theme: Theme = {
  sm : 576,
  md : 768,
  lg : 992,
  xl : 1200,
};

// Hook'us pukus
const useWindowSize = (data: ThemeBoolean) => {
  // Initialize state with undefined width/height so server and client renders match
  // Learn more here: https://joshwcomeau.com/react/the-perils-of-rehydration/

  const [ windowSize, setWindowSize ] = useState<Size>({
    width  : undefined,
    height : undefined,
  });

  useEffect(() => {
    // Handler to call on window resize
    function handleResize() {
      // Set window width/height to state
      setWindowSize({
        width  : window.innerWidth,
        height : window.innerHeight,
      });
    }

    // Add event listener
    window.addEventListener('resize', handleResize);
    // Call handler right away so state gets updated with initial window size
    handleResize();

    // Remove event listener on cleanup
    return () => window.removeEventListener('resize', handleResize);
  }, []); // Empty array ensures that effect is only run on mount

  const width = windowSize.width || 0;
  let result: DynamicType;

  if (width < theme.sm) {
    result = data.base;
  } else if (width < theme.md) {
    result = data.sm;
  } else if (width < theme.lg) {
    result = data.md;
  } else if (width < theme.xl) {
    result = data.lg;
  } else if (width > theme.xl) {
    result = data.xl;
  }

  return result;
};

export default useWindowSize;
