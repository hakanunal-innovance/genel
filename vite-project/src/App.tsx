import { Button, Form, Input, message, Space, Row, Col } from "antd";

interface Form {
  url: string;
}

const App = () => {
  const [form] = Form.useForm();
  const urlValue = Form.useWatch("url", form);

  const layout = {
    labelCol: { span: 8 },
    wrapperCol: { span: 16 },
  };

  const validateMessages = {
    required: "${label} is required!",
    types: {
      url: "${label} is not a valid url!",
      number: "${label} is not a valid number!",
    },
    string: {
      min: "${label} length must be min ${min} ",
    },
  };

  const onFinish = (data:Form) => {
    console.log(data);
    message.success("Submit success!");
  };

  const onFinishFailed = () => {
    message.error("Submit failed!");
  };

  const onFill = () => {
    form.setFieldsValue({
      url: "https://taobao.com/",
    });
  };
  return (
    <Row>
      <Col sm={{ span: 12, offset: 6 }}>
        <Form
          {...layout}
          form={form}
          layout="horizontal"
          onFinish={onFinish}
          onFinishFailed={onFinishFailed}
          autoComplete="off"
          validateMessages={validateMessages}
        >
          <Form.Item
            name="url"
            label="URL"
            rules={[
              { required: true },
              { type: "url", warningOnly: true },
              { type: "string", min: 6 },
            ]}
          >
            <Input placeholder="input placeholder" />
          </Form.Item>
          <Form.Item>
            <Space>
              <Button type="primary" htmlType="submit">
                Submit
              </Button>
              <Button htmlType="button" onClick={onFill}>
                Fill
              </Button>
            </Space>
            <div>{urlValue}</div>
          </Form.Item>
        </Form>
      </Col>
    </Row>
  );
};

export default App;
